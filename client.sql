/**
location will contain pincode, city,state,each city has unique pin code.
each staff will have one manager.
one manager will handle many staff.
each contract will contain one client,one manager,estimated cost,compleation date.
each manager can have many contract
each client can have many contract
*/
--client table
CREATE TABLE `client` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `location_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `client_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `location` (`postal_Code`)
)
--contract table
contract | CREATE TABLE `contract` (
  `id` int NOT NULL,
  `client_id` int DEFAULT NULL,
  `manager_id` int DEFAULT NULL,
  `estimated_cost` int DEFAULT NULL,
  `compleation_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  KEY `manager_id` (`manager_id`),
  CONSTRAINT `contract_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `contract_ibfk_2` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`)
)
--location table
location | CREATE TABLE `location` (
  `postal_Code` int NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state_id` int DEFAULT NULL,
  PRIMARY KEY (`postal_Code`),
  KEY `state_id` (`state_id`),
  CONSTRAINT `location_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`)
)
--manager table
CREATE TABLE `manager` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `location_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `manager_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `location` (`postal_Code`)
)
--state table
CREATE TABLE `state` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
--staff table 
CREATE TABLE `staff` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `location_id` int DEFAULT NULL,
  `manager_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`),
  KEY `manager_id` (`manager_id`),
  CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `location` (`postal_Code`),
  CONSTRAINT `staff_ibfk_2` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`)
)
