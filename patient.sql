/**
each patient treated by only one doctor and he will get one prescription
patient address consist of pincode and city
each doctor will have secretary, one secretary can have many doctors
prescription will contain patient-id,doctor-id,date,drug
one patient prescription can have one or more drugs.
each drug will have different dosage based on patient
*/
--doctor table
CREATE TABLE `doctor` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `secretary_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `secretary_id` (`secretary_id`),
  CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`secretary_id`) REFERENCES `secretary` (`id`)
)
--secretary
CREATE TABLE `secretary` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
--patient_address table
CREATE TABLE `patient_address` (
  `pincode` int NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pincode`)
)
--patient table
patient | CREATE TABLE `patient` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `address_id` int DEFAULT NULL,
  `prescription_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prescription_id` (`prescription_id`),
  CONSTRAINT `patient_ibfk_1` FOREIGN KEY (`prescription_id`) REFERENCES `prescription` (`id`)
)
--prescription table
CREATE TABLE `prescription` (
  `id` int NOT NULL,
  `Doctor_id` int DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Doctor_id` (`Doctor_id`),
  CONSTRAINT `prescription_ibfk_1` FOREIGN KEY (`Doctor_id`) REFERENCES `doctor` (`id`)
)
--drug table 
CREATE TABLE `drug` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
--join prescription and drug table
CREATE TABLE `joinPrescriptionDrug` (
  `id` int NOT NULL,
  `prescription_id` int DEFAULT NULL,
  `drug_id` int DEFAULT NULL,
  `dosage` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prescription_id` (`prescription_id`),
  KEY `drug_id` (`drug_id`),
  CONSTRAINT `joinPrescriptionDrug_ibfk_1` FOREIGN KEY (`prescription_id`) REFERENCES `prescription` (`id`),
  CONSTRAINT `joinPrescriptionDrug_ibfk_2` FOREIGN KEY (`drug_id`) REFERENCES `drug` (`id`)
)

