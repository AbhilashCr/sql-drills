-- Book table
/**
book table contains unique ISBN id ,publisher,Author,title
branch table contains name and address
one book can be published in many branch,
one branch can have many books to publish
*/
CREATE TABLE `book` (
  `ISBN` int NOT NULL,
  `Publisher` varchar(50) DEFAULT NULL,
  `Author` varchar(50) DEFAULT NULL,
  `Title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ISBN`)
)
-- Branch table
CREATE TABLE `branch` (
  `id` int NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Address` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
//join table
CREATE TABLE `joinBookBranch` (
  `branch_id` int DEFAULT NULL,
  `book_id` int DEFAULT NULL,
  KEY `branch_id` (`branch_id`),
  KEY `book_id` (`book_id`),
  CONSTRAINT `joinBookBranch_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `joinBookBranch_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `book` (`ISBN`)
)